# Easy Drive
![EasyDrive Logo](http://finance-technologie.fr/wp-content/uploads/2018/04/EASYDRIVE.png "EasyDrive Logo")
## Synopsis
Android app for communicating with the OBD Dongle on the car via Bluetooth and sending the obtained data to our API for analysis.

## Membres

* Oussama Janaheddine
* Audran Dessaint 
* Imane Largou
* Pierre Jaumain
* Roberta Liu

## Apk
The APK file of our app could be downloaded through this [Link](https://drive.google.com/file/d/1lF50Ve6HymshJiYNVztnVzmPSdMMSJXD/view?usp=sharing).

## File Format Example

```
MAT403096BNL00000, RPM, SPEED, AMBIENTAIRTEMPERATURE, THROTTLEPOSITION, ABSOLUTELOAD, 
2021-04-09 16:07:38:31,2441RPM,10km/h,27C,16.5%,0.0%,
2021-04-09 16:07:40:58,4062RPM,92km/h,27C,43.9%,40.0%,
2021-04-09 16:07:42:93,1303RPM,55km/h,27C,16.5%,16.1%,
2021-04-09 16:07:45:30,2441RPM,10km/h,27C,18.0%,73.3%,
2021-04-09 16:07:47:63,1354RPM,41km/h,27C,43.9%,23.5%,
2021-04-09 16:07:49:98,1303RPM,92km/h,27C,26.7%,22.4%,
2021-04-09 16:07:52:23,2441RPM,114km/h,27C,18.4%,43.5%,
2021-04-09 16:07:54:58,4062RPM,29km/h,27C,21.2%,33.3%,
2021-04-09 16:07:57:03,637RPM,51km/h,27C,26.7%,0.0%,
2021-04-09 16:07:59:23,4208RPM,55km/h,27C,21.2%,73.3%,
2021-04-09 16:08:01:34,1297RPM,68km/h,27C,31.4%,23.5%,
```

New version : no spaces, time in seconds since epoch, no unit
```
MAT403096BNL00000,RPM,SPEED,AMBIENTAIRTEMPERATURE,THROTTLEPOSITION,ABSOLUTELOAD,
1620116385.8784578,2441,10,27,16.5,0.0,
1620116385.8784578,4062,92,27,43.9,40.0,
1620116385.8784578,1303,55,27,16.5,16.1,
1620116385.8784578,2441,10,27,18.0,73.3,
1620116385.8784578,135,41,27,43.9,23.5,
1620116385.8784578,1303,92,27,26.7,22.4,
1620116385.8784578,2441,114,27,18.4,43.5,
1620116385.8784578,4062,29,27,21.2,33.3,
1620116385.8784578,637,51,27,26.7,0.0,
1620116385.8784578,4208,55,27,21.2,73.3,
1620116385.8784578,1297,68,27,31.4,23.5,
```

## Supported OBD Commands

### Temperature Commands
* AirIntakeTemperatureCommand
* AmbientAirTemperatureCommand
* EngineCoolantTemperatureCommand

### Pressure Commands
* BarometricPressureCommand
* FuelPressureCommand
* FuelRailPressureCommand
* IntakeManifoldPressureCommand

### Fuel Commands
* AirFuelRatioCommand
* ConsumptionRateCommand
* EthanolLevelCommand
* FindFuelTypeCommand
* FuelLevelCommand
* FuelTrimCommand
* WidebandAirFuelRatioCommand

### Engine Commands
* AbsoluteLoadCommand
* LoadCommand
* MassAirFlowCommand
* OilTempCommand
* RPMCommand
* RelativeThrottlePositionCommand
* RuntimeCommand
* SpeedCommand
* ThrottlePositionCommand

### Control Commands
* DistanceMILOnCommand
* DistanceSinceCCCommand
* DtcNumberCommand
* EquivalentRatioCommand
* IgnitionMonitorCommand
* ModuleVoltageCommand
* PendingTroubleCodesCommand
* PermanentTroubleCodesCommand
* TimeSinceCCCommand
* TimeSinceMILOnCommand
* TimingAdvanceCommand
* TroubleCodesCommand
* VinCommand
