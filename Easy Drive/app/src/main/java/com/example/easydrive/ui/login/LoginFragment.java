package com.example.easydrive.ui.login;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.easydrive.MainActivity;
import com.example.easydrive.R;
import com.example.easydrive.ui.home.HomeFragment;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;

public class LoginFragment extends Fragment {

    private LoginViewModel loginViewModel;
    private boolean login = true;
    private FirebaseAuth mAuth;
    private String vin;
    private long t0;
    private MainActivity activity;
    LinearLayout linearLayoutLoginNoProgress;
    LinearLayout linearLayoutRegisterNoProgress;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loginViewModel = new ViewModelProvider(this, new LoginViewModelFactory())
                .get(LoginViewModel.class);
        activity = (MainActivity) getActivity();
        mAuth = activity.getmAuth();
        vin = activity.getVIN();
        t0 = activity.getT0();

        final TabLayout tabLayout = view.findViewById(R.id.tabLayout);
        final LinearLayout linearLayoutLogin = view.findViewById(R.id.login_layout);
        final LinearLayout linearLayoutRegister = view.findViewById(R.id.register_layout);
        linearLayoutLoginNoProgress = view.findViewById(R.id.login_layout_noprogress);
        linearLayoutRegisterNoProgress = view.findViewById(R.id.register_layout_noprogress);
        final TextView loginMessage = view.findViewById(R.id.login_message);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getText().equals(getString(R.string.sign_up))) {
                    loginMessage.setText(R.string.register);
                    linearLayoutLogin.setVisibility(View.GONE);
                    linearLayoutRegister.setVisibility(View.VISIBLE);
                    login = false;
                } else {
                    loginMessage.setText(R.string.login);
                    linearLayoutRegister.setVisibility(View.GONE);
                    linearLayoutLogin.setVisibility(View.VISIBLE);
                    login = true;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if (tab.getText().equals(getString(R.string.sign_up))) {
                    loginMessage.setText(R.string.register);
                    linearLayoutLogin.setVisibility(View.GONE);
                    linearLayoutRegister.setVisibility(View.VISIBLE);
                    login = false;
                } else {
                    loginMessage.setText(R.string.login);
                    linearLayoutRegister.setVisibility(View.GONE);
                    linearLayoutLogin.setVisibility(View.VISIBLE);
                    login = true;
                }
            }
        });

        final EditText emailEditText = view.findViewById(R.id.email);
        final EditText passwordEditText = view.findViewById(R.id.password);
        final Button loginButton = view.findViewById(R.id.login);
        final ProgressBar loadingProgressBar = view.findViewById(R.id.loading);

        final EditText nameEditTextRegister = view.findViewById(R.id.name_register);
        final EditText emailEditTextRegister = view.findViewById(R.id.email_register);
        final EditText passwordEditTextRegister = view.findViewById(R.id.password_register);
        final Button registerButton = view.findViewById(R.id.register);
        final ProgressBar loadingProgressBarRegister = view.findViewById(R.id.loading_register);

        loginViewModel.getLoginFormState().observe(getViewLifecycleOwner(), new Observer<LoginFormState>() {
            @Override
            public void onChanged(@Nullable LoginFormState loginFormState) {
                if (loginFormState == null) {
                    return;
                }
                boolean noError = true;
                if (loginFormState.getUsernameError() != null) {
                        emailEditText.setError(getString(loginFormState.getUsernameError()));
                    noError = false;
                }
                if (loginFormState.getPasswordError() != null) {
                        passwordEditText.setError(getString(loginFormState.getPasswordError()));
                    noError = false;
                }

                if (noError) {
                        loginButton.setEnabled(true);
                        loginButton.setBackgroundColor(getResources().getColor(R.color.easy_blue));
                }
            }
        });

        loginViewModel.getRegisterFormState().observe(getViewLifecycleOwner(), new Observer<RegisterFormState>() {
            @Override
            public void onChanged(RegisterFormState registerFormState) {
                if (registerFormState == null) {
                    return;
                }
                boolean noError = true;
                if (registerFormState.getNameError() != null) {
                        nameEditTextRegister.setError(getString(registerFormState.getNameError()));
                    noError = false;
                }
                if (registerFormState.getEmailError() != null) {
                    emailEditTextRegister.setError(getString(registerFormState.getEmailError()));
                    noError = false;
                }

                if (registerFormState.getPasswordError() != null) {
                        passwordEditTextRegister.setError(getString(registerFormState.getPasswordError()));
                    noError = false;
                }

                if (noError) {
                        registerButton.setEnabled(true);
                        registerButton.setBackgroundColor(getResources().getColor(R.color.easy_blue));
                }
            }
        });

        loginViewModel.getLoginResult().observe(getViewLifecycleOwner(), new Observer<LoginResult>() {
            @Override
            public void onChanged(@Nullable LoginResult loginResult) {
                loadingProgressBar.setVisibility(View.GONE);
                linearLayoutLoginNoProgress.setVisibility(View.VISIBLE);
                if (loginResult != null) {
                    if (loginResult.getError() != null) {
                        showLoginFailed(loginResult.getError(), loginResult.getErrorMessage());
                    }
                    if (loginResult.getSuccess()) {
                        updateUiWithUser(loginResult.getSuccess());
                    }
                }

                Log.d("LOGINRESULT", "NULL");
            }
        });

        loginViewModel.getRegisterResult().observe(getViewLifecycleOwner(), new Observer<RegisterResult>() {
            @Override
            public void onChanged(RegisterResult registerResult) {
                if (registerResult == null) {
                    return;
                }
                    loadingProgressBarRegister.setVisibility(View.GONE);
                    linearLayoutRegisterNoProgress.setVisibility(View.VISIBLE);
                if (registerResult.getError() != null) {
                    showLoginFailed(registerResult.getError(),registerResult.getErrorMessage());
                }
                if (registerResult.getSuccess()) {
                    updateUiWithUser(registerResult.getSuccess());
                }
            }

        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (login) {
                    loginViewModel.loginDataChanged(emailEditText.getText().toString(),
                            passwordEditText.getText().toString());
                } else {
                    loginViewModel.registerDataChanged(nameEditTextRegister.getText().toString(),emailEditTextRegister.getText().toString(),
                            passwordEditTextRegister.getText().toString());
                }
            }
        };
        emailEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        emailEditTextRegister.addTextChangedListener(afterTextChangedListener);
        passwordEditTextRegister.addTextChangedListener(afterTextChangedListener);
        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (login) {
                        loginViewModel.login(mAuth, emailEditText.getText().toString(),
                                passwordEditText.getText().toString(), vin, t0, getContext());
                    } else {
                        loginViewModel.register(mAuth, nameEditTextRegister.getText().toString(),emailEditTextRegister.getText().toString(),
                                passwordEditTextRegister.getText().toString(),vin, t0,getContext());
                    }
                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutLoginNoProgress.setVisibility(View.GONE);
                loadingProgressBar.setVisibility(View.VISIBLE);
                loginViewModel.login(mAuth, emailEditText.getText().toString(),
                        passwordEditText.getText().toString(), vin, t0, getContext());
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutRegisterNoProgress.setVisibility(View.GONE);
                loadingProgressBarRegister.setVisibility(View.VISIBLE);
                loginViewModel.register(mAuth, nameEditTextRegister.getText().toString(),emailEditTextRegister.getText().toString(),
                        passwordEditTextRegister.getText().toString(),vin, t0,getContext());
            }
        });
    }

    private void updateUiWithUser(boolean success) {
        String welcome = getString(R.string.welcome);
        if (getContext() != null && getContext().getApplicationContext() != null) {
            Toast.makeText(getContext().getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
        }
        NavigationView navigationView= activity.findViewById(R.id.nav_view);
        Menu menu = navigationView.getMenu();
        menu.findItem(R.id.signin).setVisible(false);
        menu.findItem(R.id.logout).setVisible(true);
        activity.transitionFragment(HomeFragment.class, menu.findItem(R.id.dashboard));
    }

    private void showLoginFailed(@StringRes Integer errorString, String errorMessage) {
        if (getContext() != null && getContext().getApplicationContext() != null) {
            Toast.makeText(
                    getContext().getApplicationContext(),
                    getString(errorString) + errorMessage,
                    Toast.LENGTH_LONG).show();
        }
    }
}