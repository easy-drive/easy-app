package com.example.easydrive.ui.login;

import android.content.Context;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.util.Log;
import android.util.Patterns;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.easydrive.MySingleton;
import com.example.easydrive.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public class LoginViewModel extends ViewModel {
    private String url = "http://easydrive.cs-campus.fr/api/";
    private final MutableLiveData<LoginFormState> loginFormState = new MutableLiveData<>();
    private final MutableLiveData<RegisterFormState> registerFormState = new MutableLiveData<>();
    private final MutableLiveData<LoginResult> loginResult = new MutableLiveData<>();
    private final MutableLiveData<RegisterResult> registerResult = new MutableLiveData<>();

    LoginViewModel() {

    }

    LiveData<LoginFormState> getLoginFormState() {
        return loginFormState;
    }
    LiveData<RegisterFormState> getRegisterFormState() { return registerFormState;}

    LiveData<LoginResult> getLoginResult() {
        return loginResult;
    }
    LiveData<RegisterResult> getRegisterResult() {
        return registerResult;
    }

    public void login(FirebaseAuth mAuth,String email, String password, String vin, long t0, Context context) {
        mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    loginResult.setValue(new LoginResult(true));
                } else {
                    loginResult.setValue(new LoginResult(R.string.login_failed, task.getException().getMessage()));
                }
            }
        });
    }

    public void loginDataChanged(String email, String password) {
        if (!isEmailValid(email)) {
            loginFormState.setValue(new LoginFormState(R.string.invalid_email, null));
        } else if (!isPasswordValid(password)) {
            loginFormState.setValue(new LoginFormState(null, R.string.invalid_password));
        } else {
            loginFormState.setValue(new LoginFormState(true));
        }
    }

    // A placeholder username validation check
    private boolean isEmailValid(String username) {
        if (username == null) {
            return false;
        }
        return Patterns.EMAIL_ADDRESS.matcher(username).matches();
    }

    // A placeholder password validation check
    private boolean isPasswordValid(String password) {
        return password != null && password.trim().length() > 5;
    }

    public void registerDataChanged(String name, String email, String password) {
        if (!isNameValid(name)) {
            registerFormState.setValue(new RegisterFormState(R.string.invalid_name,null, null));
        } else if (!isEmailValid(email)) {
            registerFormState.setValue(new RegisterFormState(null,R.string.invalid_email, null));
        } else if (!isPasswordValid(password)) {
            registerFormState.setValue(new RegisterFormState(null,null, R.string.invalid_password));
        } else {
            registerFormState.setValue(new RegisterFormState(true));
        }
    }

    private boolean isNameValid(String name) {
        if (name == null) {
            return false;
        }
        return !name.trim().isEmpty();
    }
    private final String TAG  = "REGISTER";
    public void register(FirebaseAuth mAuth, String name, String email, String password, String vin, long t0, Context context) {
        Log.d(TAG,vin);
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener( new OnCompleteListener<AuthResult>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            String id = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();
                            try {
                                JSONObject params = new JSONObject();
                                params.put("user_id",id);
                                params.put("name",name);
                                params.put("email",email);
                                params.put("car_id",vin);
                                params.put("time", t0);
                                Log.d(TAG,params.toString());
                                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                                        (Request.Method.POST, url + "register",params, new Response.Listener<JSONObject>() {

                                            @Override
                                            public void onResponse(JSONObject response) {
                                                        registerResult.setValue(new RegisterResult(true));
                                            }
                                        }, new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                // TODO: Handle error
                                                registerResult.setValue(new RegisterResult(R.string.login_failed,error.getMessage()));
                                                mAuth.getCurrentUser().delete();
                                            }
                                        }) {};

                                // Access the RequestQueue through your singleton class.
                                MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
                            } catch (JSONException e) {
                                registerResult.setValue(new RegisterResult(R.string.login_failed,e.getMessage()));
                                mAuth.getCurrentUser().delete();
                            }
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            registerResult.setValue(new RegisterResult(R.string.login_failed, Objects.requireNonNull(task.getException()).getMessage()));
                        }
                    }
                });
    }
}