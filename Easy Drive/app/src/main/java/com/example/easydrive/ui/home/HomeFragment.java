package com.example.easydrive.ui.home;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.easydrive.MainActivity;
import com.example.easydrive.R;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private MainActivity mainActivity;
    private View root;

    @RequiresApi(api = Build.VERSION_CODES.M)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        root = inflater.inflate(R.layout.fragment_home, container, false);
        return root;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mainActivity = (MainActivity) getActivity();
        mainActivity.connectionStatus(mainActivity.getSharedPreferences(getString(R.string.bluetooth_preference_key), Context.MODE_PRIVATE), getString(R.string.connection_status), root);
    }
}