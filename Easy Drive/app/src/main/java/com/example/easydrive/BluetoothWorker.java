package com.example.easydrive;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import org.jetbrains.annotations.NotNull;

public class BluetoothWorker extends Worker {
    private final Context context;

    public BluetoothWorker(
            @NonNull Context context,
            @NonNull WorkerParameters params) {
        super(context, params);
        this.context = context;
    }

    @Override
    public @NotNull Result doWork() {
        String device = context.getSharedPreferences(context.getString(R.string.bluetooth_preference_key), Context.MODE_PRIVATE).getString(context.getString(R.string.connected_device), "NULL_VALUE");
        Intent serviceIntent = new Intent(context, BluetoothService.class);
        serviceIntent.putExtra("DEVICE", device);
        context.startForegroundService(serviceIntent);

        // Indicate whether the work finished successfully with the Result
        return Result.success();
    }
}