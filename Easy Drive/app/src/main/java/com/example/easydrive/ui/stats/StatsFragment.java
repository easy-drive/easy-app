package com.example.easydrive.ui.stats;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.easydrive.MainActivity;
import com.example.easydrive.R;
import com.example.easydrive.MySingleton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class StatsFragment extends Fragment {

    private String url = "http://easydrive.cs-campus.fr/api/";
    private StatsViewModel statsViewModel;
    private ScrollView scrollView;
    private SwipeRefreshLayout swipeRefresh;
    private TextView securityMark;
    private TextView efficiencyMark;
    private TextView remark;
    private TextView mark;
    private TextView cost;
    private TextView speed;
    private TextView duration;
    private TextView distance;
    private ProgressBar securityLoading;
    private ProgressBar efficiencyLoading;
    private ProgressBar remarkLoading;
    private ProgressBar markLoading;
    private ProgressBar costLoading;
    private ProgressBar speedLoading;
    private ProgressBar durationLoading;
    private ProgressBar distanceLoading;
    private TextView securityLabel;
    private TextView efficiencyLabel;
    private TextView markLabel;
    private TextView costLabel;
    private TextView speedLabel;
    private MainActivity activity;
    private DrawerLayout drawerLayout;
    private FirebaseAuth mAuth;
    private String vin;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        statsViewModel =
                new ViewModelProvider(this).get(StatsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_stats, container, false);
        return root;
    }

    public void onViewCreated(View view,Bundle savedInstanceState) {
        swipeRefresh = view.findViewById(R.id.swipeRefresh);
        scrollView = view.findViewById(R.id.scrollViewStats);
        securityMark = view.findViewById(R.id.securityMark);
        efficiencyMark = view.findViewById(R.id.efficiencyMark);
        remark = view.findViewById(R.id.remark);
        mark = view.findViewById(R.id.mark);
        cost = view.findViewById(R.id.cost);
        speed = view.findViewById(R.id.speed);
        duration = view.findViewById(R.id.duration);
        distance = view.findViewById(R.id.distance);

        securityLabel = view.findViewById(R.id.securityLabel);
        efficiencyLabel = view.findViewById(R.id.efficiencyLabel);
        markLabel = view.findViewById(R.id.markLabel);
        costLabel = view.findViewById(R.id.costLabel);
        speedLabel = view.findViewById(R.id.speedLabel);

        securityLoading = view.findViewById(R.id.securityLoading);
        efficiencyLoading = view.findViewById(R.id.efficiencyLoading);
        remarkLoading = view.findViewById(R.id.remarkLoading);
        markLoading = view.findViewById(R.id.markLoading);
        costLoading = view.findViewById(R.id.costLoading);
        speedLoading = view.findViewById(R.id.speedLoading);
        durationLoading = view.findViewById(R.id.durationLoading);
        distanceLoading = view.findViewById(R.id.distanceLoading);

        activity = (MainActivity) getActivity();
        mAuth = activity.getmAuth();
        vin = activity.getVIN();
        drawerLayout = activity.getDrawerLayout();

        startLoading();

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                startLoading();
            }
        });
    }

    private void startLoading() {
        securityMark.setVisibility(View.INVISIBLE);
        efficiencyMark.setVisibility(View.INVISIBLE);
        remark.setVisibility(View.INVISIBLE);
        mark.setVisibility(View.INVISIBLE);
        cost.setVisibility(View.INVISIBLE);
        speed.setVisibility(View.INVISIBLE);
        duration.setVisibility(View.INVISIBLE);
        distance.setVisibility(View.INVISIBLE);

        securityLabel.setVisibility(View.INVISIBLE);
        efficiencyLabel.setVisibility(View.INVISIBLE);
        markLabel.setVisibility(View.INVISIBLE);
        costLabel.setVisibility(View.INVISIBLE);
        speedLabel.setVisibility(View.INVISIBLE);

        securityLoading.setVisibility(View.VISIBLE);
        efficiencyLoading.setVisibility(View.VISIBLE);
        remarkLoading.setVisibility(View.VISIBLE);
        markLoading.setVisibility(View.VISIBLE);
        costLoading.setVisibility(View.VISIBLE);
        speedLoading.setVisibility(View.VISIBLE);
        durationLoading.setVisibility(View.VISIBLE);
        distanceLoading.setVisibility(View.VISIBLE);

        if (mAuth.getCurrentUser() == null) {
            stopLoading(false);
            Snackbar.make(drawerLayout, R.string.login_prompt, Snackbar.LENGTH_SHORT).show();
        } else {
            try {
                JSONObject params = new JSONObject();
                params.put("user_id", mAuth.getCurrentUser().getUid());
                params.put("car_id", vin);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                        (Request.Method.POST, url + "grades",params, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    stopLoading(true);
                                    updateDisplay(response);
                                } catch (JSONException e) {
                                    stopLoading(false);
                                    Snackbar.make(drawerLayout, "An error happened, please try again.", Snackbar.LENGTH_SHORT).show();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                stopLoading(false);
                                Snackbar.make(drawerLayout, "An error happened, please try again.", Snackbar.LENGTH_SHORT).show();
                            }
                        }) {};

                // Access the RequestQueue through your singleton class.
                MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
            } catch (JSONException e) {
                stopLoading(false);
                Snackbar.make(drawerLayout, "An error happened, please try again.", Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    private void stopLoading(boolean success) {
        swipeRefresh.setRefreshing(false);
        securityLoading.setVisibility(View.INVISIBLE);
        efficiencyLoading.setVisibility(View.INVISIBLE);
        remarkLoading.setVisibility(View.INVISIBLE);
        markLoading.setVisibility(View.INVISIBLE);
        costLoading.setVisibility(View.INVISIBLE);
        speedLoading.setVisibility(View.INVISIBLE);
        durationLoading.setVisibility(View.INVISIBLE);
        distanceLoading.setVisibility(View.INVISIBLE);

        int visibility;
        if (success) {
            visibility = View.VISIBLE;
        } else {
            visibility = View.INVISIBLE;
        }

        securityLabel.setVisibility(visibility);
        efficiencyLabel.setVisibility(visibility);
        markLabel.setVisibility(visibility);
        costLabel.setVisibility(visibility);
        speedLabel.setVisibility(visibility);

        securityMark.setVisibility(visibility);
        efficiencyMark.setVisibility(visibility);
        remark.setVisibility(visibility);
        mark.setVisibility(visibility);
        cost.setVisibility(visibility);
        speed.setVisibility(visibility);
        duration.setVisibility(visibility);
        distance.setVisibility(visibility);
    }

    private void updateDisplay(JSONObject response) throws JSONException {
        JSONArray columns = response.getJSONArray("columns");
        JSONArray data = response.getJSONArray("processed_data");

        double grade = (double)data.get(16);
        double securityGrade = (double) data.get(15);
        double efficiencyGrade = (double) data.get(10);
        double costPrice = (double) data.get(6);
        double avgSpeed = (double) data.get(2);
        double rideTime = (double) data.get(5);
        double rideDistance = (double) data.get(4);

        int hours = (int) rideTime;
        int minutes = (int)((rideTime-hours)*60);
        int seconds = (int)((rideTime-hours)*3600 - minutes*60);

        String securityMarkText = "" + (int)(securityGrade*100);
        String efficiencyMarkText = "" + (int)(efficiencyGrade*100);
        String markText = "" + (int)(grade*100);
        String costText;
        if (costPrice < 10) {
            costText = String.format("%.02f", costPrice)+"€";
        } else if (costPrice < 100) {
            costText = String.format("%.01f", costPrice)+"€";
        } else if (costPrice < 1000) {
            costText = (int)costPrice +"€";
        } else {
            costText = String.format("%.01f", costPrice/1000)+"k€";
        }
        Log.d("CostPrice", String.format("%.01f", 19.59));
        String speedText = (int)avgSpeed + "Km/h";
        String durationText;
        if (hours == 0)  {
            if (minutes == 0) {
                durationText = "Ride duration: " + String.format("%02ds", seconds);
            } else {
                durationText = "Ride duration: " + String.format("%02dm %02ds", minutes, seconds);
            }
        } else {
            durationText = "Ride duration: " + String.format("%dh %02dm %02ds", hours, minutes, seconds);
        }
        String distanceText;

        if (rideDistance >= 10) {
            distanceText = "Ride mileage: " + String.format("%.02f", rideDistance) + " Km";
        } else {
            distanceText = "Ride mileage: " +  (int)(rideDistance*1000) + " m";
        }
        String remarkText;

        if (grade <= 0.2){
           remarkText = "Very bad driving. Driver needs to improve immediately!";
        } else if (0.2 < grade && grade <= 0.4) {
            remarkText = "Bad driving. Driver must improve.";
        } else if (0.4 < grade && grade <= 0.6) {
            remarkText = "Regular driving, however it could be improved.";
        } else if (0.6 < grade && grade <= 0.8) {
            remarkText = "Good driving, although it could be slightly improved.";
        } else {
            remarkText = "Excellent driving!";
        }
        securityMark.setText(securityMarkText);
        efficiencyMark.setText(efficiencyMarkText);
        remark.setText(remarkText);
        mark.setText(markText);
        cost.setText(costText);
        speed.setText(speedText);
        duration.setText(durationText);
        distance.setText(distanceText);
    }
}