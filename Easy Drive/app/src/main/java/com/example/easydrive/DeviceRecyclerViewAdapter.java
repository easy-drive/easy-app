package com.example.easydrive;

import android.bluetooth.BluetoothDevice;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

public class DeviceRecyclerViewAdapter
        extends RecyclerView.Adapter<DeviceRecyclerViewAdapter.ViewHolder>
        implements BluetoothDiscoveryDeviceListener {

    /**
     * The devices shown in this {@link RecyclerView}.
     */
    private final List<BluetoothDevice> devices;
    private final LinkedHashSet<BluetoothDevice> hashSet;
    private final ListItemListener<BluetoothDevice> listener;
    public ViewHolder viewHolder;
    private BluetoothController bluetooth;

    public DeviceRecyclerViewAdapter(ListItemListener<BluetoothDevice> listener) {
        this.devices = new ArrayList<>();
        this.hashSet = new LinkedHashSet<>();
        this.listener = listener;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public @NotNull ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_device_item, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = devices.get(position);
        holder.mImageView.setImageResource(getDeviceIcon(devices.get(position)));
        holder.mDeviceNameView.setText(devices.get(position).getName());
        holder.mDeviceAddressView.setText(devices.get(position).getAddress());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    try {
                        listener.onItemClick(holder.mItem);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    /**
     * Returns the icon shown on the left of the device inside the list.
     *
     * @param device the device for the icon to get.
     */
    private int getDeviceIcon(BluetoothDevice device) {
        if (bluetooth.isAlreadyPaired(device)) {
            return R.drawable.ic_bluetooth_connected_black_24;
        } else {
            return R.drawable.ic_bluetooth_black_24;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getItemCount() {
        return devices.size();
    }

    /**
     * {@inheritDoc}
     */
    public void onDeviceDiscovered(BluetoothDevice device) {
        listener.endLoading(true);
        if (hashSet.add(device)) devices.add(device);
        notifyDataSetChanged();
    }

    /**
     * {@inheritDoc}
     */
    public void onDeviceDiscoveryStarted() {
        cleanView();
        listener.startLoading();
    }

    /**
     * Cleans the view.
     */
    public void cleanView() {
        devices.clear();
        hashSet.clear();
        notifyDataSetChanged();
    }


    @Override
    public void setBluetoothController(BluetoothController bluetooth) {
        this.bluetooth = bluetooth;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDeviceDiscoveryEnd() {
        listener.endLoading(false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBluetoothStatusChanged() {
        // Notifies the Bluetooth controller.
        bluetooth.onBluetoothStatusChanged();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBluetoothTurningOn() {
        listener.startLoading();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDevicePairingEnded() throws IOException {
        if (bluetooth.isPairingInProgress()) {
            BluetoothDevice device = bluetooth.getBoundingDevice();
            switch (bluetooth.getPairingDeviceStatus()) {
                case BluetoothDevice.BOND_BONDING:
                    // Still pairing, do nothing.
                    break;
                case BluetoothDevice.BOND_BONDED:
                    // Successfully paired.
                    listener.endLoadingWithDialog(false, device);
                    // Updates the icon for this element.
                    notifyDataSetChanged();
                    break;
                case BluetoothDevice.BOND_NONE:
                    // Failed pairing.
                    listener.endLoadingWithDialog(true, device);
                    break;
            }
        }
    }

    @Override
    public void onConnectionAttempt(String result) {
        listener.onConnectionAttempt(result);
    }

    public BluetoothDevice getDevice(View v) {
        TextView device_address = v.findViewById(R.id.device_address);
        String deviceAddress = device_address.getText().toString();
        for (BluetoothDevice device : devices) {
            if (device.getAddress() == deviceAddress) {
                return device;
            }
        }
        return null;
    }

    /**
     * ViewHolder for a BluetoothDevice.
     */
    class ViewHolder extends RecyclerView.ViewHolder {

        /**
         * The inflated view of this ViewHolder.
         */
        final View mView;

        /**
         * The icon of the device.
         */
        final ImageView mImageView;

        /**
         * The name of the device.
         */
        final TextView mDeviceNameView;

        /**
         * The MAC address of the BluetoothDevice.
         */
        final TextView mDeviceAddressView;

        /**
         * The item of this ViewHolder.
         */
        BluetoothDevice mItem;

        /**
         * Instantiates a new ViewHolder.
         *
         * @param view the inflated view of this ViewHolder.
         */
        ViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = view.findViewById(R.id.device_icon);
            mDeviceNameView = view.findViewById(R.id.device_name);
            mDeviceAddressView = view.findViewById(R.id.device_address);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + BluetoothController.deviceToString(mItem) + "'";
        }
    }
}

