package com.example.easydrive.ui.login;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

/**
 * Authentication result : success (user details) or error message.
 */
class RegisterResult {
    private boolean success;
    private Integer error;
    private String errorMessage;

    RegisterResult(Integer error, String errorMessage) {
        this.error = error;
        this.errorMessage = errorMessage;
    }

    RegisterResult(boolean success) {
        this.success = success;
    }

    boolean getSuccess() {
        return success;
    }

    Integer getError() {
        return error;
    }

    String getErrorMessage() { return errorMessage;}
}