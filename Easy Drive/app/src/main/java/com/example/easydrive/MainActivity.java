package com.example.easydrive;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.example.easydrive.ui.about.AboutFragment;
import com.example.easydrive.ui.home.HomeFragment;
import com.example.easydrive.ui.login.LoginFragment;
import com.example.easydrive.ui.settings.SettingsFragment;
import com.example.easydrive.ui.stats.StatsFragment;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private FirebaseAuth mAuth;
    public Toolbar toolbar;
    public BroadcastReceiver mReceiver;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private SharedPreferences.OnSharedPreferenceChangeListener listener;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Configure all views

        configureToolBar();

        configureDrawerLayout();

        configureNavigationView();

        listener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                if (key.equals(getString(R.string.connection_status))) {
                    try {
                        connectionStatus(sharedPreferences, key);
                    } catch (Throwable ignored) {
                    }
                }
            }
        };

        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.bluetooth_preference_key), MODE_PRIVATE);
        sharedPreferences.registerOnSharedPreferenceChangeListener(listener);

        String CONNECTION_STATUS = sharedPreferences.getString(getString(R.string.connection_status), "DISCONNECTED");
        //connectionStatus(getSharedPreferences(getString(R.string.bluetooth_preference_key),MODE_PRIVATE),getString(R.string.connection_status));
        if (!StartActivity.isLocationEnabled(this) && CONNECTION_STATUS.equals("CONNECTED")) {
            displayLocationSettingsRequest(this);
        }

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        Menu menu = navigationView.getMenu();
        boolean visible = false;
        if(currentUser == null){
            Snackbar.make(drawerLayout, R.string.login_prompt, Snackbar.LENGTH_SHORT)
                    .show();
            visible = true;
        }
        menu.findItem(R.id.signin).setVisible(visible);
        menu.findItem(R.id.logout).setVisible(!visible);
        if (CONNECTION_STATUS.equals("DISCONNECTED")) {
            menu.findItem(R.id.disconnect).setVisible(false);
        } else {
            menu.findItem(R.id.disconnect).setVisible(true);
        }
    }

    @Override
    public void onBackPressed() {
        // 5 - Handle back click to close menu
        if (this.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressLint("NonConstantResourceId")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // 4 - Handle Navigation Item Click
        int id = item.getItemId();
        SharedPreferences sharedPrefs = getSharedPreferences(getString(R.string.bluetooth_preference_key), MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = sharedPrefs.edit();
        Intent btIntent = new Intent(MainActivity.this, StartActivity.class);
        switch (id) {
            case R.id.dashboard:
                transitionFragment(HomeFragment.class, item);
                break;
            case R.id.stats:
                transitionFragment(StatsFragment.class, item);
                break;
            case R.id.settings:
                transitionFragment(SettingsFragment.class, item);
                break;
            case R.id.about:
                transitionFragment(AboutFragment.class, item);
                break;
            case R.id.signin:
                transitionFragment(LoginFragment.class,item);
                break;
            case R.id.disconnect:
                prefEditor.putString(getString(R.string.connection_status), "DISCONNECTED");
                //prefEditor.putString(getString(R.string.connected_device),"NULL_VALUE");
                prefEditor.putBoolean(getString(R.string.disconnection_attempt), true);
                prefEditor.apply();
                stopService(new Intent(MainActivity.this, BluetoothService.class));
                item.setVisible(false);
                break;
            case R.id.unbond:
                prefEditor.putString(getString(R.string.connection_status), "DISCONNECTED");
                prefEditor.putString(getString(R.string.connected_device), "NULL_VALUE");
                prefEditor.apply();
                stopService(new Intent(MainActivity.this, BluetoothService.class));
                startActivity(btIntent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_in_left);
                finish();
                break;
            case R.id.logout:
                try {
                    FirebaseAuth.getInstance().signOut();
                } catch (Throwable ignored) {}
                Snackbar.make(drawerLayout, "Logged out", Snackbar.LENGTH_SHORT)
                        .show();
                Menu menu = navigationView.getMenu();
                item.setVisible(false);
                menu.findItem(R.id.signin).setVisible(true);
                transitionFragment(HomeFragment.class, menu.findItem(R.id.dashboard));
                break;
            default:
                break;
        }

        this.drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
            getSharedPreferences(getString(R.string.bluetooth_preference_key), MODE_PRIVATE).unregisterOnSharedPreferenceChangeListener(listener);
        } catch (Throwable ignored) {
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onResume() {
        super.onResume();
        if (!StartActivity.isLocationEnabled(this)) {
            displayLocationSettingsRequest(this);
        }
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.bluetooth_preference_key), MODE_PRIVATE);
        connectionStatus(sharedPreferences, getString(R.string.connection_status));
        sharedPreferences.registerOnSharedPreferenceChangeListener(listener);
    }


    @Override
    protected void onPause() {
        super.onPause();
        getSharedPreferences(getString(R.string.bluetooth_preference_key), MODE_PRIVATE).unregisterOnSharedPreferenceChangeListener(listener);
    }


    // ---------------------
    // CONFIGURATION
    // ---------------------

    // 1 - Configure Toolbar
    private void configureToolBar() {
        this.toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    // 2 - Configure Drawer Layout
    private void configureDrawerLayout() {
        this.drawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
    }

    // 3 - Configure NavigationView
    private void configureNavigationView() {
        this.navigationView = findViewById(R.id.nav_view);
        Menu menu = navigationView.getMenu();
        navigationView.setNavigationItemSelectedListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void connectionStatus(SharedPreferences sharedPreferences, String key) {
        connectionStatus(sharedPreferences, key, null);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void connectionStatus(SharedPreferences sharedPreferences, String key, View view) {
        Button connectButton;
        ImageView appIcon;
        if (view != null) {
            connectButton = view.findViewById(R.id.connectButton);
            appIcon = view.findViewById(R.id.appIcon);
        } else {
            connectButton = findViewById(R.id.connectButton);
            appIcon = findViewById(R.id.appIcon);
        }
        TextView titleView = findViewById(R.id.toolbarTitle);
        Log.d("TitleVIEW", String.valueOf(titleView));
        if (sharedPreferences.getString(key, "DISCONNECTED").equals("DISCONNECTED")) {
            titleView.setText(getString(R.string.disconnected));
            toolbar.setBackgroundColor(getColor(R.color.easy_red));

            crossfade(appIcon, "OUT");
            crossfade(connectButton, "IN");

            connectButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String deviceAddress = getSharedPreferences(getString(R.string.bluetooth_preference_key), MODE_PRIVATE).getString(getString(R.string.connected_device), "NULL_VALUE");
                    final ProgressDialog[] connectingProgressDialog = {ProgressDialog.show(MainActivity.this, "", "Connecting to device " + deviceAddress + "...", true, false)};
                    Intent serviceIntent = new Intent(MainActivity.this, BluetoothService.class);
                    serviceIntent.putExtra("DEVICE", deviceAddress);
                    LocalBroadcastManager.getInstance(MainActivity.this).unregisterReceiver(mReceiver);
                    mReceiver = new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {
                            String result = intent.getStringExtra("RESULT");
                            if (result.equals("SUCCESS")) {
                                // Dismisses the progress dialog and prints a message to the user.
                                if (connectingProgressDialog[0] != null && connectingProgressDialog[0].isShowing()) {
                                    connectingProgressDialog[0].dismiss();
                                }
                                getNavigationView().getMenu().findItem(R.id.disconnect).setVisible(true);
                                Toast.makeText(context, "Connected successfully to device " + deviceAddress, Toast.LENGTH_SHORT).show();
                                // Cleans up state.
                                connectingProgressDialog[0] = null;
                            } else if (result.equals("FAILED")) {
                                // Dismisses the progress dialog and prints a message to the user.
                                if (connectingProgressDialog[0] != null && connectingProgressDialog[0].isShowing()) {
                                    connectingProgressDialog[0].dismiss();
                                }
                                getNavigationView().getMenu().findItem(R.id.disconnect).setVisible(false);
                                Toast.makeText(context, "Failed to connect to device " + deviceAddress, Toast.LENGTH_SHORT).show();
                                // Cleans up state.
                                connectingProgressDialog[0] = null;
                            }
                        }
                    };
                    LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(mReceiver, new IntentFilter(getString(R.string.connection_attempt_status)));
                    startService(serviceIntent);
                }
            });

        } else {
            titleView.setText(R.string.connected);
            toolbar.setBackgroundColor(getColor(R.color.easy_blue));
            crossfade(connectButton, "OUT");
            crossfade(appIcon, "IN");
        }
    }


    private void displayLocationSettingsRequest(Context context) {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(this).checkLocationSettings(builder.build());

        ((Task) result).addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.

                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                // Cast to a resolvable exception.
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                resolvable.startResolutionForResult(
                                        MainActivity.this,
                                        LocationRequest.PRIORITY_HIGH_ACCURACY);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            } catch (ClassCastException e) {
                                // Ignore, should be an impossible error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            }
        });

    }

    public void crossfade(View test, String in) {
        int shortAnimationDuration = getResources().getInteger(
                android.R.integer.config_mediumAnimTime);
        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        try  {
            if (in.equals("IN")) {
                test.setAlpha(0f);
                test.setVisibility(View.VISIBLE);

                // Animate the content view to 100% opacity, and clear any animation
                // listener set on the view.
                test.animate()
                        .alpha(1f)
                        .setDuration(shortAnimationDuration)
                        .setListener(null);
            } else {
                test.animate()
                        .alpha(0f)
                        .setDuration(shortAnimationDuration)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                test.setVisibility(View.GONE);
                            }
                        });

            }
        } catch (NullPointerException e) {

        }
    }

    public void transitionFragment(Class fragmentClass, MenuItem item) {
        Fragment fragment = null;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.nav_host_fragment, fragment).commit();

        item.setChecked(true);

        // Set action bar title
        setTitle(item.getTitle());
    }

    public FirebaseAuth getmAuth() { return mAuth;}

    public String getVIN() {
        return getSharedPreferences(getString(R.string.bluetooth_preference_key),MODE_PRIVATE).getString(getString(R.string.vin),"");
    }

    public long getT0() {
        return getSharedPreferences(getString(R.string.bluetooth_preference_key),MODE_PRIVATE).getLong(getString(R.string.t0),System.currentTimeMillis());
    }

    public DrawerLayout getDrawerLayout() { return drawerLayout; }
    public NavigationView getNavigationView() { return navigationView;}
}