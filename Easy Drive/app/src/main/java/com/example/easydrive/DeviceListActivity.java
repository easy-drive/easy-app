package com.example.easydrive;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class DeviceListActivity extends AppCompatActivity implements ListItemListener<BluetoothDevice> {
    private static final String TAG = "DeviceListActivity";
    DeviceRecyclerViewAdapter recyclerViewAdapter;
    RecyclerViewEmptySupport recyclerView;
    private ProgressDialog bondingProgressDialog;
    private BluetoothController bluetooth;
    private FloatingActionButton fab;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_list);
        Toolbar myToolbar = findViewById(R.id.toolbar2);
        setSupportActionBar(myToolbar);
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
        recyclerViewAdapter = new DeviceRecyclerViewAdapter(this);
        recyclerView = findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        // Sets the view to show when the dataset is empty. IMPORTANT : this method must be called
        // before recyclerView.setAdapter().
        View emptyView = findViewById(R.id.empty_list);
        recyclerView.setEmptyView(emptyView);
        // Sets the view to show during progress.
        ProgressBar progressBar = findViewById(R.id.progressBar);
        recyclerView.setProgressView(progressBar);
        recyclerView.setAdapter(recyclerViewAdapter);
        this.bluetooth = new BluetoothController(this, BluetoothAdapter.getDefaultAdapter(), recyclerViewAdapter);
        LocalBroadcastManager.getInstance(this).registerReceiver(bluetooth.getBroadcastReceiverDelegator(), new IntentFilter(getString(R.string.connection_attempt_status)));
        fab = findViewById(R.id.fab);
        if (!bluetooth.isDiscovering()) {
            // Starts the discovery.
            Snackbar.make(recyclerView, R.string.device_discovery_started, Snackbar.LENGTH_SHORT).show();
            bluetooth.startDiscovery();
        } else {
            Snackbar.make(recyclerView, R.string.device_discovery_stopped, Snackbar.LENGTH_SHORT).show();
            bluetooth.cancelDiscovery();
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!bluetooth.isDiscovering()) {
                    // Starts the discovery.
                    Snackbar.make(recyclerView, R.string.device_discovery_started, Snackbar.LENGTH_SHORT).show();
                    bluetooth.startDiscovery();
                } else {
                    Snackbar.make(recyclerView, R.string.device_discovery_stopped, Snackbar.LENGTH_SHORT).show();
                    bluetooth.cancelDiscovery();
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onItemClick(BluetoothDevice device) throws IOException {
        Log.d(TAG, "Item clicked : " + BluetoothController.deviceToString(device));
        if (device != null) {
            // Prints a message to the user.
            String deviceName = BluetoothController.getDeviceName(device);
            if (device.getBondState() == BluetoothDevice.BOND_BONDED) {
                bluetooth.setConnectingDevice(device);
                bondingProgressDialog = ProgressDialog.show(this, "", "Connecting to device " + deviceName + "...", true, false);
                Intent serviceIntent = new Intent(this, BluetoothService.class);
                serviceIntent.putExtra("DEVICE", device.getAddress());
                startService(serviceIntent);
            } else {
                Log.d(TAG, "Device not paired. Pairing.");
                boolean outcome = false;
                try {
                    outcome = bluetooth.pair(device);
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }

                if (outcome) {
                    // The pairing has started, shows a progress dialog.
                    Log.d(TAG, "Showing pairing dialog");
                    bondingProgressDialog = ProgressDialog.show(this, "", "Pairing with the device " + deviceName + "...", true, false);
                } else {
                    Log.d(TAG, "Error when pairing with the device " + deviceName + "!");
                    Toast.makeText(this, "Error when pairing with the device " + deviceName + "!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        bluetooth.close();
        Intent myIntent = new Intent(getApplicationContext(), StartActivity.class);
        startActivityForResult(myIntent, 0);
        overridePendingTransition(R.anim.slide_out_left, R.anim.slide_out_right);
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        bluetooth.close();

        Intent myIntent = new Intent(getApplicationContext(), StartActivity.class);
        startActivityForResult(myIntent, 0);
        overridePendingTransition(R.anim.slide_out_left, R.anim.slide_out_right);
        finish();
    }

    @Override
    public void startLoading() {
        if (!bluetooth.bluetooth.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, 1);
            onActivityResult(1, 0, enableBtIntent);
        } else if (!StartActivity.isLocationEnabled(this)) {
            displayLocationSettingsRequest(this);
        } else if (StartActivity.isLocationEnabled(this) && bluetooth.bluetooth.isEnabled()) {
            recyclerView.startLoading();

            // Changes the button icon.
            fab.setImageResource(R.drawable.ic_bluetooth_searching_white_24);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // check if the request code is same as what is passed  here it is 1
        if (requestCode == 1) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                if (!StartActivity.isLocationEnabled(this)) {
                    displayLocationSettingsRequest(this);
                } else if (StartActivity.isLocationEnabled(this) && bluetooth.bluetooth.isEnabled()) {
                    recyclerView.startLoading();

                    // Changes the button icon.
                    fab.setImageResource(R.drawable.ic_bluetooth_searching_white_24);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void endLoading(boolean partialResults) {
        this.recyclerView.endLoading();

        // If discovery has ended, changes the button icon.
        if (!partialResults) {
            fab.setImageResource(R.drawable.ic_bluetooth_white_24);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void endLoadingWithDialog(boolean error, BluetoothDevice device) throws IOException {
        if (this.bondingProgressDialog != null) {
            View view = findViewById(R.id.empty_list);
            String message;
            String deviceName = BluetoothController.getDeviceName(device);
            // Gets the message to print.
            if (error) {
                message = "Failed pairing with device " + deviceName + "!";
                // Dismisses the progress dialog and prints a message to the user.
                if (bondingProgressDialog != null && bondingProgressDialog.isShowing()) {
                    bondingProgressDialog.dismiss();
                }
                Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show();
                // Cleans up state.
                bondingProgressDialog = null;
            } else {
                bluetooth.setConnectingDevice(device);
                bondingProgressDialog.setMessage("Connecting to the device " + deviceName + "...");
                Intent serviceIntent = new Intent(this, BluetoothService.class);
                serviceIntent.putExtra("DEVICE", device.getAddress());
                startService(serviceIntent);
            }
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onDestroy() {
        bluetooth.close();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(bluetooth.getBroadcastReceiverDelegator());
        super.onDestroy();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRestart() {
        super.onRestart();
        // Stops the discovery.
        if (this.bluetooth != null) {
            this.bluetooth.cancelDiscovery();
        }
        // Cleans the view.
        if (this.recyclerViewAdapter != null) {
            this.recyclerViewAdapter.cleanView();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onStop() {
        super.onStop();
        // Stoops the discovery.
        if (this.bluetooth != null) {
            this.bluetooth.cancelDiscovery();
        }
    }

    @Override
    public void transition(String activity) {
        bluetooth.close();
        Intent intent;
        switch (activity) {
            case "MAIN":
                intent = new Intent(this, MainActivity.class);
                break;
            case "START":
                intent = new Intent(getApplicationContext(), StartActivity.class);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + activity);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_in_left);
        finish();
    }

    @Override
    public void onConnectionAttempt(String result) {
        if (result.equals("SUCCESS")) {
            // Dismisses the progress dialog and prints a message to the user.
            if (bondingProgressDialog != null && bondingProgressDialog.isShowing()) {
                bondingProgressDialog.dismiss();
            }
            Toast.makeText(this, "Connected successfully to device " + bluetooth.getConnectingDeviceName(), Toast.LENGTH_SHORT).show();
            // Cleans up state.
            bondingProgressDialog = null;
            bluetooth.setConnectingDevice(null);
            transition("MAIN");
        } else if (result.equals("FAILED")) {
            // Dismisses the progress dialog and prints a message to the user.
            if (bondingProgressDialog != null && bondingProgressDialog.isShowing()) {
                bondingProgressDialog.dismiss();
            }
            Toast.makeText(this, "Failed to connect to device " + bluetooth.getConnectingDeviceName(), Toast.LENGTH_SHORT).show();
            // Cleans up state.
            bondingProgressDialog = null;
            bluetooth.setConnectingDevice(null);
        }
    }


    private void displayLocationSettingsRequest(Context context) {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(DeviceListActivity.this).checkLocationSettings(builder.build());

        ((Task) result).addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    if (bluetooth.bluetooth.isEnabled()) {
                        recyclerView.startLoading();

                        // Changes the button icon.
                        fab.setImageResource(R.drawable.ic_bluetooth_searching_white_24);
                    }
                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                // Cast to a resolvable exception.
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                resolvable.startResolutionForResult(
                                        DeviceListActivity.this,
                                        LocationRequest.PRIORITY_HIGH_ACCURACY);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            } catch (ClassCastException e) {
                                // Ignore, should be an impossible error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            }
        });

    }

}
