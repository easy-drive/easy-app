package com.example.easydrive;

import android.Manifest;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class StartActivity extends AppCompatActivity {
    final int REQUEST_ENABLE_BT = 1;
    BluetoothAdapter bluetoothAdapter;
    Button button;
    BroadcastReceiver bluetoothReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, @org.jetbrains.annotations.NotNull Intent intent) {
//            String action = intent.getAction();
//
//            if (bluetoothAdapter.R.equals(action)) {
//                if (bluetoothAdapter.isEnabled()) {
//                    displayLocationSettingsRequest(getApplicationContext());
//                }
//            }
        }
    };

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        try {
            locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
            return false;
        }

        return locationMode != Settings.Secure.LOCATION_MODE_OFF;


    }

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        SharedPreferences sharedPref = getSharedPreferences(
                getString(R.string.bluetooth_preference_key), Context.MODE_PRIVATE);
        String CONNECTED_DEVICE = sharedPref.getString(getString(R.string.connected_device), "NULL_VALUE");
        if (CONNECTED_DEVICE.equals("NULL_VALUE")) {
            setContentView(R.layout.activity_start);
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};

                ActivityCompat.requestPermissions(StartActivity.this, permissions,
                        1);
            }
            button = findViewById(R.id.button);
            button.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.P)
                public void onClick(View v) {
                    // Code here executes on main thread after user presses button
                    bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                    bluetoothAdapter.cancelDiscovery();
                    if (bluetoothAdapter != null) {
                        // Device does support Bluetooth
                        if (!bluetoothAdapter.isEnabled()) {
                            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                            onActivityResult(REQUEST_ENABLE_BT, 0, enableBtIntent);
                        } else if (!isLocationEnabled(getApplicationContext())) {
                            displayLocationSettingsRequest(getApplicationContext());
                        } else if (isLocationEnabled(getApplicationContext()) && bluetoothAdapter.isEnabled()) {
                            Intent listIntent = new Intent(StartActivity.this, DeviceListActivity.class);
                            transition(listIntent);
                        }
                    }

                }
            });
        } else {
            Intent mainIntent = new Intent(StartActivity.this, MainActivity.class);
            transition(mainIntent);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // check if the request code is same as what is passed  here it is 1
        if (requestCode == REQUEST_ENABLE_BT) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                if (!isLocationEnabled(getApplicationContext())) {
                    displayLocationSettingsRequest(getApplicationContext());
                } else if (isLocationEnabled(getApplicationContext()) && bluetoothAdapter.isEnabled()) {
                    Intent listIntent = new Intent(StartActivity.this, DeviceListActivity.class);
                    transition(listIntent);
                }
            }
        }
    }

    private void displayLocationSettingsRequest(Context context) {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(StartActivity.this).checkLocationSettings(builder.build());

        ((Task) result).addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    if (bluetoothAdapter.isEnabled()) {
                        Intent listIntent = new Intent(StartActivity.this, DeviceListActivity.class);
                        transition(listIntent);
                    }
                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                // Cast to a resolvable exception.
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                resolvable.startResolutionForResult(
                                        StartActivity.this,
                                        LocationRequest.PRIORITY_HIGH_ACCURACY);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            } catch (ClassCastException e) {
                                // Ignore, should be an impossible error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            }
        });

    }

    private void transition(Intent intent) {
        try {
            unregisterReceiver(bluetoothReceiver);
        } catch (Throwable ignored) {
        }
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_in_left);
        finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(bluetoothReceiver);
        } catch (Throwable ignored) {
        }
    }
}
