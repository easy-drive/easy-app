package com.example.easydrive.ui.login;

/**
 * Authentication result : success (user details) or error message.
 */
class LoginResult {
    private boolean success;
    private Integer error;
    private String errorMessage;

    LoginResult(Integer error, String errorMessage) {
        this.error = error;
        this.errorMessage = errorMessage;
    }

    LoginResult(boolean success) {
        this.success = success;
    }

    boolean getSuccess() {
        return success;
    }

    Integer getError() {
        return error;
    }

    String getErrorMessage() { return errorMessage;}
}