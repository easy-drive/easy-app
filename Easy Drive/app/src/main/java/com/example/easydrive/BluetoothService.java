package com.example.easydrive;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.icu.text.SimpleDateFormat;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import androidx.work.WorkRequest;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import br.ufrn.imd.obd.commands.ObdCommand;
import br.ufrn.imd.obd.commands.ObdCommandGroup;
import br.ufrn.imd.obd.commands.control.DistanceMILOnCommand;
import br.ufrn.imd.obd.commands.control.DistanceSinceCCCommand;
import br.ufrn.imd.obd.commands.control.DtcNumberCommand;
import br.ufrn.imd.obd.commands.control.EquivalentRatioCommand;
import br.ufrn.imd.obd.commands.control.IgnitionMonitorCommand;
import br.ufrn.imd.obd.commands.control.ModuleVoltageCommand;
import br.ufrn.imd.obd.commands.control.PendingTroubleCodesCommand;
import br.ufrn.imd.obd.commands.control.PermanentTroubleCodesCommand;
import br.ufrn.imd.obd.commands.control.TimeSinceCCCommand;
import br.ufrn.imd.obd.commands.control.TimeSinceMILOnCommand;
import br.ufrn.imd.obd.commands.control.TimingAdvanceCommand;
import br.ufrn.imd.obd.commands.control.TroubleCodesCommand;
import br.ufrn.imd.obd.commands.control.VinCommand;
import br.ufrn.imd.obd.commands.engine.AbsoluteLoadCommand;
import br.ufrn.imd.obd.commands.engine.LoadCommand;
import br.ufrn.imd.obd.commands.engine.MassAirFlowCommand;
import br.ufrn.imd.obd.commands.engine.OilTempCommand;
import br.ufrn.imd.obd.commands.engine.RPMCommand;
import br.ufrn.imd.obd.commands.engine.RelativeThrottlePositionCommand;
import br.ufrn.imd.obd.commands.engine.RuntimeCommand;
import br.ufrn.imd.obd.commands.engine.SpeedCommand;
import br.ufrn.imd.obd.commands.engine.ThrottlePositionCommand;
import br.ufrn.imd.obd.commands.fuel.AirFuelRatioCommand;
import br.ufrn.imd.obd.commands.fuel.ConsumptionRateCommand;
import br.ufrn.imd.obd.commands.fuel.EthanolLevelCommand;
import br.ufrn.imd.obd.commands.fuel.FindFuelTypeCommand;
import br.ufrn.imd.obd.commands.fuel.FuelLevelCommand;
import br.ufrn.imd.obd.commands.fuel.FuelTrimCommand;
import br.ufrn.imd.obd.commands.fuel.WidebandAirFuelRatioCommand;
import br.ufrn.imd.obd.commands.pressure.BarometricPressureCommand;
import br.ufrn.imd.obd.commands.pressure.FuelPressureCommand;
import br.ufrn.imd.obd.commands.pressure.FuelRailPressureCommand;
import br.ufrn.imd.obd.commands.pressure.IntakeManifoldPressureCommand;
import br.ufrn.imd.obd.commands.protocol.EchoOffCommand;
import br.ufrn.imd.obd.commands.protocol.LineFeedOffCommand;
import br.ufrn.imd.obd.commands.protocol.SelectProtocolCommand;
import br.ufrn.imd.obd.commands.protocol.TimeoutCommand;
import br.ufrn.imd.obd.commands.temperature.AirIntakeTemperatureCommand;
import br.ufrn.imd.obd.commands.temperature.AmbientAirTemperatureCommand;
import br.ufrn.imd.obd.commands.temperature.EngineCoolantTemperatureCommand;
import br.ufrn.imd.obd.enums.ObdProtocols;

public class BluetoothService extends Service {
    // SPP UUID service - this should work for most devices
    private String url = "http://easydrive.cs-campus.fr/api/";
    private static UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private final String filename = "EasyDriveData";
    private String VIN;
    private float FUELLEVEL;
    private int i = 0;
    private final long COLLECTING_INTERVAL = 1000;  // milliseconds
    // String for MAC address
    private static String MAC_ADDRESS;
    final int handlerState = 0;                        //used to identify handler message
    private final int FILE_LIMIT = 200;
    private final long INTERVAL = 5000;
    private final long initialDelay = 100;             // Milliseconds
    private final long longRepeatTimeInterval = 15;    // Minutes
    private final StringBuilder recDataString = new StringBuilder();
    private ArrayList<String> unsent = new ArrayList<String>();
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(LocationManager.MODE_CHANGED_ACTION)) {
                if (!StartActivity.isLocationEnabled(context)) {
                    Log.d("GPS", "Turned Off");
                    gpsNotification();
                } else {
                    Log.d("GPS", "Turned On");
                    NotificationManagerCompat.from(context).cancel(Integer.parseInt(getString(R.string.turnOnGPSNotificationId)));
                }
            }
        }
    };
    Handler bluetoothIn;
    private BluetoothAdapter btAdapter = null;
    private ConnectingThread mConnectingThread;
    private ConnectedThread mConnectedThread;
    private boolean stopThread;
    private ArrayList<ObdCommand> Commands;
    private ObdCommandGroup obdCommands;
    private FirebaseAuth mAuth;
    private Double latitude;
    private Double longitude;
    private Double altitude;
    private FusedLocationProviderClient fusedLocationProviderClient;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("BT SERVICE", "SERVICE CREATED");
        stopThread = false;
        IntentFilter gpsFilter = new IntentFilter(LocationManager.MODE_CHANGED_ACTION);
        registerReceiver(mReceiver, gpsFilter);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @SuppressLint({"HandlerLeak", "HardwareIds"})
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("BT SERVICE", "SERVICE STARTED");
        btAdapter = BluetoothAdapter.getDefaultAdapter(); // get Bluetooth adapter
        if (btAdapter == null) {
            stopSelf();
        } else if (!btAdapter.isEnabled()) {
            btAdapter.enable();
        }
        MAC_ADDRESS = btAdapter.getAddress();
        String deviceAddress = null;
        if (intent != null) {
            deviceAddress = intent.getStringExtra("DEVICE");
        }
        if (deviceAddress == null) {
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.bluetooth_preference_key), MODE_PRIVATE);
            deviceAddress = sharedPreferences.getString(getString(R.string.device_mac_address), "00:00:00:00:00:00");
        }
        BluetoothDevice device = btAdapter.getRemoteDevice(deviceAddress);

        TimerTask myTask = new TimerTask() {
            public void run() {
                Log.d("TIMER", "TIME IS UP");
                SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.bluetooth_preference_key), MODE_PRIVATE);
                String CONNECTION_STATUS = sharedPreferences.getString(getString(R.string.connection_status), "DISCONNECTED");
                Log.d("TIMER", CONNECTION_STATUS);
                if (CONNECTION_STATUS.equals("DISCONNECTED")) {
                    resultSequence("FAILED");
                    stopSelf();
                }
            }
        };
        Timer myTimer = new Timer();
        myTimer.schedule(myTask, INTERVAL);
        connectingNotification();
        try {
            BTMODULEUUID = device.getUuids()[0].getUuid();
            Log.d("UUID", BTMODULEUUID.toString());
        } catch (Throwable ignored) {

        }
        BluetoothDevice finalDevice = device;
        bluetoothIn = new Handler(Looper.getMainLooper()) {
            public void handleMessage(android.os.Message msg) {
                Log.d("DEBUG", "handleMessage");
                if (msg.what == handlerState) { //if message is what we want
                    if (!StartActivity.isLocationEnabled(BluetoothService.this)) {
                        gpsNotification();
                    }
                    String readMessage = (String) msg.obj; // msg.arg1 = bytes from connect thread
                    recDataString.append(readMessage);//`enter code here`
                    Log.d("RECORDED", recDataString.toString());
                    // Do stuff here with your data, like adding it to the database
                    resultSequence("SUCCESS");
                    connectedNotification(intent, finalDevice);
                }
                recDataString.delete(0, recDataString.length()); //clear all string data
            }
        };
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.bluetooth_preference_key), MODE_PRIVATE);
        i = sharedPreferences.getInt(getString(R.string.file_number), 0);
        ArrayList<String> tempUnsent = new ArrayList<String>();
        for (int k = 0; k < i; k++) {
            tempUnsent.add(String.valueOf(k));
        }
        unsent.addAll(sharedPreferences.getStringSet(getString(R.string.unsent_array), new HashSet<String>(tempUnsent)));
        checkBTState(device);
        sendData();
        return super.onStartCommand(intent, flags, startId);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (bluetoothIn != null) {
            bluetoothIn.removeCallbacksAndMessages(null);
        }
        stopThread = true;
        try {
            if (mConnectedThread != null) {
                mConnectedThread.closeStreams();
            }
        } catch (Throwable ignored) {

        }
        try {
            if (mConnectingThread != null) {
                mConnectingThread.closeSocket();
            }
        } catch (Throwable ignored) {

        }

        try {
            unregisterReceiver(mReceiver);
        } catch (Throwable ignored) {
        }

        Log.d("SERVICE", "onDestroy");
        disconnectedNotification();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    //Checks that the Android device Bluetooth is available and prompts to be turned on if off
    private void checkBTState(BluetoothDevice device) {

        if (btAdapter == null) {
            Log.d("BT SERVICE", "BLUETOOTH NOT SUPPORTED BY DEVICE, STOPPING SERVICE");
            stopSelf();
        } else {
            if (btAdapter.isEnabled()) {
                try {
                    //Log.d("DEBUG BT", "ATTEMPTING TO CONNECT TO REMOTE DEVICE : " + device.toString());
                    mConnectingThread = new ConnectingThread(device);
                    mConnectingThread.start();
                } catch (IllegalArgumentException e) {
                    Log.d("DEBUG BT", "PROBLEM WITH MAC ADDRESS : " + e.toString());
                    Log.d("BT SEVICE", "ILLEGAL MAC ADDRESS, STOPPING SERVICE");
                    resultSequence("FAILED");
                    stopSelf();
                }
            } else {
                Log.d("BT SERVICE", "BLUETOOTH NOT ON, STOPPING SERVICE");
                stopSelf();
            }
        }
    }

    private void resultSequence(String result) {
        Intent intent = new Intent(getString(R.string.connection_attempt_status));
        // You can also include some extra data.
        intent.putExtra("RESULT", result);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void gpsNotification() {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(this, 0, notificationIntent, 0);
        Bitmap largeIcon = drawableToBitmap(R.mipmap.ic_launcher_round);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), getString(R.string.channel_id))
                .setContentTitle(getText(R.string.notification_title_turn_on))
                .setContentText(getText(R.string.notification_turn_on_message))
                .setSmallIcon(R.drawable.ic_notification)
                .setLargeIcon(largeIcon)
                .setContentIntent(pendingIntent)
                .setTicker(getText(R.string.ticker_text_turn_on))
                .setAutoCancel(true);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(Integer.parseInt(getString(R.string.turnOnGPSNotificationId)), builder.build());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void connectedNotification(Intent intent, BluetoothDevice device) {
        SharedPreferences sharedPrefs = getSharedPreferences(getString(R.string.bluetooth_preference_key), MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = sharedPrefs.edit();
        prefEditor.putString(getString(R.string.connection_status), "CONNECTED");
        prefEditor.putString(getString(R.string.connected_device), device.toString());
        prefEditor.putInt(getString(R.string.connection_attempt_number), 0);
        prefEditor.apply();
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(BluetoothService.this);
        notificationManagerCompat.cancelAll();
        WorkManager.getInstance(getApplicationContext()).cancelAllWork();
        String input = intent.getStringExtra("inputExtra");
        final String CHANNEL_ID = getString(R.string.channel_id);
        NotificationChannel serviceChannel = new NotificationChannel(
                CHANNEL_ID,
                "Foreground Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT);

        getSystemService(NotificationManager.class).createNotificationChannel(serviceChannel);
        Intent notificationIntent = new Intent(getApplicationContext(), StartActivity.class);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, 0);
        Bitmap largeIcon = drawableToBitmap(R.mipmap.ic_launcher_round);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID)
                .setContentTitle(getText(R.string.notification_title_connected))
                .setContentText(getText(R.string.notification_message_connected))
                .setSmallIcon(R.drawable.ic_notification)
                .setLargeIcon(largeIcon)
                .setContentIntent(pendingIntent)
                .setTicker(getText(R.string.ticker_text_connected))
                .setColorized(true)
                .setColor(getColor(R.color.easy_blue));
        Notification notification = builder.build();

        startForeground(Integer.parseInt(getString(R.string.connectionNotificationId)), notification);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void connectingNotification() {
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(BluetoothService.this);
        notificationManagerCompat.cancelAll();
        WorkManager.getInstance(getApplicationContext()).cancelAllWork();
        final String CHANNEL_ID = getString(R.string.channel_id);
        NotificationChannel serviceChannel = new NotificationChannel(
                CHANNEL_ID,
                "Foreground Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT);

        getSystemService(NotificationManager.class).createNotificationChannel(serviceChannel);
        Intent notificationIntent = new Intent(getApplicationContext(), StartActivity.class);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, 0);
        Bitmap largeIcon = drawableToBitmap(R.mipmap.ic_launcher_round);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID)
                .setContentTitle(getText(R.string.notification_title_connected))
                .setContentText(getText(R.string.notification_message_connecting))
                .setSmallIcon(R.drawable.ic_notification)
                .setLargeIcon(largeIcon)
                .setContentIntent(pendingIntent)
                .setTicker(getText(R.string.ticker_text_connecting))
                .setColorized(true)
                .setColor(getColor(R.color.easy_blue));
        Notification notification = builder.build();

        startForeground(Integer.parseInt(getString(R.string.connectionNotificationId)), notification);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void disconnectedNotification() {
        SharedPreferences sharedPrefs = getSharedPreferences(getString(R.string.bluetooth_preference_key), MODE_PRIVATE);
        String CONNECTION_STATUS = sharedPrefs.getString(getString(R.string.connection_status), "DISCONNECTED");
        boolean DISCONNECTION_ATTEMPT = sharedPrefs.getBoolean(getString(R.string.disconnection_attempt), false);
        String CONNECTED_DEVICE = sharedPrefs.getString(getString(R.string.connected_device), "NULL_VALUE");
        Log.d("DISCONNECTION_ATTEMPT", String.valueOf(DISCONNECTION_ATTEMPT));
        Log.d("CONNECTED_DEVICE", CONNECTED_DEVICE);
        SharedPreferences.Editor prefEditor = sharedPrefs.edit();
        prefEditor.putString(getString(R.string.connection_status), "DISCONNECTED");
        prefEditor.putInt(getString(R.string.file_number), i);
        if (!CONNECTED_DEVICE.equals("NULL_VALUE") && !DISCONNECTION_ATTEMPT) {
            int i = sharedPrefs.getInt(getString(R.string.connection_attempt_number), 0);
            if (i == 0) {
                WorkManager.getInstance(this).cancelAllWorkByTag(getString(R.string.work_tag));
                WorkRequest bluetoothWorkRequest =
                        new PeriodicWorkRequest.Builder(BluetoothWorker.class, longRepeatTimeInterval, TimeUnit.MINUTES).setInitialDelay(initialDelay, TimeUnit.MILLISECONDS).addTag(getString(R.string.work_tag)).build();
                WorkManager
                        .getInstance(this)
                        .enqueue(bluetoothWorkRequest);

                prefEditor.putInt(getString(R.string.connection_attempt_number), 1);
            } else if (i == 1) {
                Intent notificationIntent = new Intent(this, StartActivity.class);
                PendingIntent pendingIntent =
                        PendingIntent.getActivity(this, 0, notificationIntent, 0);
                Bitmap largeIcon = drawableToBitmap(R.mipmap.ic_launcher_round);
                NotificationCompat.Builder builder = new NotificationCompat.Builder(this, getString(R.string.channel_id))
                        .setContentTitle(getText(R.string.notification_title_disconnected))
                        .setContentText(getText(R.string.notification_disconnect_message))
                        .setSmallIcon(R.drawable.ic_notification)
                        .setLargeIcon(largeIcon)
                        .setContentIntent(pendingIntent)
                        .setTicker(getText(R.string.ticker_text_disconnected))
                        .setAutoCancel(true);
                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

                // notificationId is a unique int for each notification that you must define
                notificationManager.notify(Integer.parseInt(getString(R.string.disconnectionNotificationId)), builder.build());
                prefEditor.putInt(getString(R.string.connection_attempt_number), 2);
            }
        }
        prefEditor.putBoolean(getString(R.string.connection_attempt_failed), true);
        prefEditor.putBoolean(getString(R.string.disconnection_attempt), false);
        prefEditor.apply();
    }

    public Bitmap drawableToBitmap(int resId) {
        Drawable drawable = getResources().getDrawable(resId);
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    private void temperatureCommands(ArrayList<ObdCommand> C) {
        C.add(new AirIntakeTemperatureCommand());
        C.add(new AmbientAirTemperatureCommand());
        C.add(new EngineCoolantTemperatureCommand());
    }

    private void pressureCommands(ArrayList<ObdCommand> C) {
        C.add(new BarometricPressureCommand());
        C.add(new FuelPressureCommand());
        C.add(new FuelRailPressureCommand());
        C.add(new IntakeManifoldPressureCommand());
    }

    private void fuelCommands(ArrayList<ObdCommand> C) {
        C.add(new AirFuelRatioCommand());
        C.add(new ConsumptionRateCommand());
        C.add(new EthanolLevelCommand());
        C.add(new FindFuelTypeCommand());
        C.add(new FuelLevelCommand());
        C.add(new FuelTrimCommand());
        C.add(new WidebandAirFuelRatioCommand());
    }

    private void engineCommands(ArrayList<ObdCommand> C) {
        C.add(new AbsoluteLoadCommand());
        C.add(new LoadCommand());
        C.add(new MassAirFlowCommand());
        C.add(new OilTempCommand());
        C.add(new RPMCommand());
        C.add(new RelativeThrottlePositionCommand());
        C.add(new RuntimeCommand());
        C.add(new SpeedCommand());
        C.add(new ThrottlePositionCommand());
    }

    private void controlCommands(ArrayList<ObdCommand> C) {
        C.add(new DistanceMILOnCommand());
        C.add(new DistanceSinceCCCommand());
        C.add(new DtcNumberCommand());
        C.add(new EquivalentRatioCommand());
        C.add(new IgnitionMonitorCommand());
        C.add(new ModuleVoltageCommand());
        C.add(new PendingTroubleCodesCommand());
        C.add(new PermanentTroubleCodesCommand());
        C.add(new TimeSinceCCCommand());
        C.add(new TimeSinceMILOnCommand());
        C.add(new TimingAdvanceCommand());
        C.add(new TroubleCodesCommand());
        C.add(new VinCommand());
    }

    private void removeAllCommands(ArrayList<ObdCommand> C, ObdCommandGroup obdCommandGroup) {
        ArrayList<ObdCommand> rmElements = new ArrayList<ObdCommand>();
        for (ObdCommand command : C) {
            obdCommandGroup.remove(command);
            rmElements.add(command);
        }
        C.removeAll(rmElements);
    }

    private void addAllCommands(ArrayList<ObdCommand> C, ObdCommandGroup obdCommandGroup) {
        for (ObdCommand command : C) {
            obdCommandGroup.add(command);
        }
    }

    private void setupOBD(InputStream mmInStream, OutputStream mmOutStream) {
        obdCommands = new ObdCommandGroup();
        Commands = new ArrayList<ObdCommand>();
        // Group many obd commands into a single command ()
        Commands.add(new EchoOffCommand());
        Commands.add(new LineFeedOffCommand());
        Commands.add(new TimeoutCommand(125));
        Commands.add(new SelectProtocolCommand(ObdProtocols.AUTO));
        Commands.add(new VinCommand());

        addAllCommands(Commands, obdCommands);

        // Run all commands at once
        try {
            Log.d("OBD COMMAND", "Starting ...");
            obdCommands.run(mmInStream, mmOutStream);
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.bluetooth_preference_key), MODE_PRIVATE);
            SharedPreferences.Editor prefEditor = sharedPreferences.edit();
            i = getSharedPreferences(getString(R.string.bluetooth_preference_key), MODE_PRIVATE).getInt(getString(R.string.file_number), 0);
            VIN = obdCommands.getFormattedResult();
            VIN = VIN.substring(VIN.length() - 18, VIN.length() - 1);
            File file = new File(getFilesDir(), filename + i);
            String headers = "V" + VIN + ",RPM,SPEED,AMBIENTAIRTEMPERATURE,THROTTLEPOSITION,ABSOLUTELOAD,FUELLEVEL,\n";
            FUELLEVEL = (float) (50 * (1 + Math.random()));
            if (file.length() > 1024 - headers.getBytes().length) {
                sendData();
                unsent.add(String.valueOf(i));
                if (i < FILE_LIMIT) {
                    i++;
                } else {
                    i = 0;
                }
                file = new File(getFilesDir(), filename + i);
                while (unsent.contains(String.valueOf(i))) {
                    if (i < FILE_LIMIT) {
                        i++;
                    } else {
                        handleFileLimit();
                    }
                    file = new File(getFilesDir(), filename + i);
                }
                try (FileOutputStream fos = new FileOutputStream(file, false)) {
                    fos.write(headers.getBytes());
                }
            } else {
                try (FileOutputStream fos = new FileOutputStream(file, true)) {
                    fos.write(headers.getBytes());
                }
            }
            String lastVIN = sharedPreferences.getString(getString(R.string.vin), "");
            if (!lastVIN.equals(VIN)) {
                long time = System.currentTimeMillis();
                prefEditor.putLong(getString(R.string.t0), time);
            }
            prefEditor.putString(getString(R.string.vin), VIN);
            prefEditor.apply();
            Log.d("VIN", VIN);
        } catch (IOException | InterruptedException e) {
            stopSelf();
        }
    }

    @SuppressLint("DefaultLocale")
    @RequiresApi(api = Build.VERSION_CODES.N)
    private void dataCollect(InputStream mmInStream, OutputStream mmOutStream) throws IOException, InterruptedException {
        removeAllCommands(Commands, obdCommands);
        Commands.add(new RPMCommand());
        Commands.add(new SpeedCommand());
        Commands.add(new AmbientAirTemperatureCommand());
        Commands.add(new ThrottlePositionCommand());
        Commands.add(new AbsoluteLoadCommand());
        //Commands.add(new FuelLevelCommand());

        addAllCommands(Commands, obdCommands);
        obdCommands.run(mmInStream, mmOutStream);
        String results = obdCommands.getFormattedResult();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        } else {
            fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            Location currentLocation = task.getResult();
                            if (currentLocation != null) {
                                latitude = currentLocation.getLatitude();
                                longitude = currentLocation.getLongitude();
                                altitude = currentLocation.getAltitude();
                            } else {
                                latitude = null;
                                longitude = null;
                                altitude = null;
                            }
                        }
                    }
                }
            );
        }
        long time = System.currentTimeMillis();
        float delta = 60000;
        float consomation = (float) 0.00000000001;
        FUELLEVEL = (float) (FUELLEVEL*Math.exp(- delta*consomation*i));
        if (latitude != null) {
            results = time + "," + latitude + "," + longitude + "," + altitude + "," + results + String.format("%.01f", FUELLEVEL) + "%,\n";
        } else {
            results = time + ",null,null,null," + results + String.format("%.01f", FUELLEVEL) + "%,\n";
        }
        Log.d("Time",results);
        File file = new File(getFilesDir(), filename + i);
        int KB = 1024;
        if (file.length() >= KB) {
            sendData();
            unsent.add(String.valueOf(i));
            if (i < FILE_LIMIT) {
                i++;
            } else {
                i = 0;
            }
            file = new File(getFilesDir(),filename + i);
            while (unsent.contains(String.valueOf(i))) {
                if (i < FILE_LIMIT) {
                    i++;
                } else {
                    handleFileLimit();
                }
                file = new File(getFilesDir(),filename + i);
            }
            try (FileOutputStream fos = new FileOutputStream(file,false)) {
                String headers = "V"+ VIN + ",RPM,SPEED,AMBIENTAIRTEMPERATURE,THROTTLEPOSITION,ABSOLUTELOAD,FUELLEVEL,\n";
                fos.write(headers.getBytes());
            }

        }
        try (FileOutputStream fos = new FileOutputStream(file, true)) {
            fos.write(results.getBytes());
        }
        Log.d("OBD RESULTS", String.valueOf(i));
    }

    // New Class for Connecting Thread
    private class ConnectingThread extends Thread {
        private final BluetoothDevice mmDevice;
        private BluetoothSocket mmSocket;

        public ConnectingThread(BluetoothDevice device) {
            Log.d("DEBUG BT", "IN CONNECTING THREAD");
            mmDevice = device;
            BluetoothSocket temp = null;
            Log.d("DEBUG BT", "MAC ADDRESS : " + MAC_ADDRESS);
            Log.d("DEBUG BT", "BT UUID : " + BTMODULEUUID);
            try {
                temp = mmDevice.createRfcommSocketToServiceRecord(BTMODULEUUID);
                Log.d("DEBUG BT", "SOCKET CREATED : " + temp.toString());
            } catch (IOException e) {
                Log.d("DEBUG BT", "SOCKET CREATION FAILED :" + e.toString());
                Log.d("BT SERVICE", "SOCKET CREATION FAILED, STOPPING SERVICE");
                stopSelf();
            }
            mmSocket = temp;
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void run() {
            super.run();
            Log.d("DEBUG BT", "IN CONNECTING THREAD RUN");
            // Establish the Bluetooth socket connection.
            // Cancelling discovery as it may slow down connection
            btAdapter.cancelDiscovery();
            String message = "FAILED";
            try {
                mmSocket.connect();
                Log.d("DEBUG BT", "BT SOCKET CONNECTED");
                mConnectedThread = new ConnectedThread(mmSocket);
                mConnectedThread.start();
                Log.d("DEBUG BT", "CONNECTED THREAD STARTED");
                //I send a character when resuming.beginning transmission to check device is connected
                //If it is not an exception will be thrown in the write method and finish() will be called
                message = "SUCCESS";
            } catch (IOException e) {
                Log.d("BT DEBUG", "There was an error while establishing Bluetooth connection. Falling back.." + e.toString());
                Class<?> clazz = mmSocket.getRemoteDevice().getClass();
                Class<?>[] paramTypes = new Class<?>[]{Integer.TYPE};
                try {
                    Method m = clazz.getMethod("createRfcommSocket", paramTypes);
                    Object[] params = new Object[]{Integer.valueOf(1)};
                    BluetoothSocket sockFallback = (BluetoothSocket) m.invoke(mmSocket.getRemoteDevice(), params);
                    sockFallback.connect();
                    mmSocket = sockFallback;
                    mConnectedThread = new ConnectedThread(mmSocket);
                    mConnectedThread.start();
                    Log.d("DEBUG BT", "CONNECTED THREAD STARTED");
                    message = "SUCCESS";
                } catch (NoSuchMethodException noSuchMethodException) {
                    noSuchMethodException.printStackTrace();
                } catch (IllegalAccessException illegalAccessException) {
                    illegalAccessException.printStackTrace();
                } catch (InvocationTargetException invocationTargetException) {
                    invocationTargetException.printStackTrace();
                } catch (IOException e1) {
                    try {
                        Log.d("DEBUG BT", "SOCKET CONNECTION FAILED : " + e1.toString());
                        Log.d("BT SERVICE", "SOCKET CONNECTION FAILED, STOPPING SERVICE");
                        mmSocket.close();
                        stopSelf();
                    } catch (IOException e2) {
                        Log.d("DEBUG BT", "SOCKET CLOSING FAILED :" + e2.toString());
                        Log.d("BT SERVICE", "SOCKET CLOSING FAILED, STOPPING SERVICE");
                        stopSelf();
                        //insert code to deal with this
                    }
                }
            } catch (IllegalStateException e) {
                Log.d("DEBUG BT", "CONNECTED THREAD START FAILED : " + e.toString());
                Log.d("BT SERVICE", "CONNECTED THREAD START FAILED, STOPPING SERVICE");
                stopSelf();
            }
            if (message.equals("FAILED")) {
                resultSequence(message);
            }
        }

        public void closeSocket() {
            try {
                //Don't leave Bluetooth sockets open when leaving activity
                mmSocket.close();
            } catch (IOException e2) {
                //insert code to deal with this
                Log.d("DEBUG BT", e2.toString());
                Log.d("BT SERVICE", "SOCKET CLOSING FAILED, STOPPING SERVICE");
                stopSelf();
            }
        }
    }

    // New Class for Connected Thread
    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private final ArrayList<String> AvailablePIDs = new ArrayList<String>();

        //creation of the connect thread
        public ConnectedThread(BluetoothSocket socket) {
            Log.d("DEBUG BT", "IN CONNECTED THREAD");
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                //Create I/O streams for connection
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.d("DEBUG BT", e.toString());
                Log.d("BT SERVICE", "UNABLE TO READ/WRITE, STOPPING SERVICE");
                stopSelf();
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;

            setupOBD(mmInStream, mmOutStream);
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        public void run() {
            Log.d("DEBUG BT", "IN CONNECTED THREAD RUN");
            boolean sent = false;
            String results = "";
            // Keep looping to listen for received messages
            while (!stopThread) {
                try {
                    // Collect data, store it and send it to API
                    dataCollect(mmInStream, mmOutStream);
                } catch (InterruptedException | IOException e) {
                    Log.d("DEBUG BT", e.toString());
                    Log.d("BT SERVICE", "UNABLE TO READ/WRITE, STOPPING SERVICE");
                    stopSelf();
                    break;
                }
                if (!sent) {
                    bluetoothIn.obtainMessage(handlerState, 0, -1, "CONNECTED").sendToTarget();
                    sent = true;
                }
                try {
                    Thread.sleep(COLLECTING_INTERVAL);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        //write method
        public void write(String input) {
            byte[] msgBuffer = input.getBytes();           //converts entered String into bytes
            try {
                mmOutStream.write(msgBuffer);                //write bytes over BT connection via outstream
            } catch (IOException e) {
                //if you cannot write, close the application
                Log.d("DEBUG BT", "UNABLE TO READ/WRITE " + e.toString());
                Log.d("BT SERVICE", "UNABLE TO READ/WRITE, STOPPING SERVICE");
                stopSelf();
            }
        }

        public void closeStreams() {
            try {
                //Don't leave Bluetooth sockets open when leaving activity
                mmInStream.close();
                mmOutStream.close();
            } catch (IOException e2) {
                //insert code to deal with this
                Log.d("DEBUG BT", e2.toString());
                Log.d("BT SERVICE", "STREAM CLOSING FAILED, STOPPING SERVICE");
                stopSelf();
            }
        }
    }

    private void sendData()  {
        Log.d("UNSENT_ARRAY", String.valueOf(unsent));
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            for (int l = 0; l < unsent.size(); l++) {
                String j = unsent.get(l);
                try {
                    File file = new File(getFilesDir(), filename + j);
                    BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
                    String read;
                    StringBuilder builder = new StringBuilder("");
                    Log.d("Upload", "SENT");
                    while ((read = bufferedReader.readLine()) != null) {
                        builder.append(read).append("\n");
                    }
                    bufferedReader.close();
                    JSONObject params = new JSONObject();
                    params.put("data", builder.toString());
                    params.put("user_id", currentUser.getUid());
                    params.put("time", System.currentTimeMillis());
                    String finalJ = j;
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                            (Request.Method.POST, url + "push", params, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.d("UPLOAD_SUCCESS", finalJ);
                                    Log.d("VALUE OF I", String.valueOf(i));
                                    unsent.remove(finalJ);
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // TODO: Handle error
                                    unsent.add(finalJ);
                                }
                            }) {};

                    // Access the RequestQueue through your singleton class.
                    MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.bluetooth_preference_key), MODE_PRIVATE);
            SharedPreferences.Editor prefEditor = sharedPreferences.edit();
            prefEditor.putInt(getString(R.string.file_number), i);
            prefEditor.putStringSet(getString(R.string.unsent_array), new HashSet<String>(unsent));
            prefEditor.apply();
        } else {
            Log.d("UNCONNECTED", "Please Connect");
        }
    }

    private void handleFileLimit() {
        i = 0;
    }

}